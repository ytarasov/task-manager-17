package ru.t1.ytarasov.tm.command;

import ru.t1.ytarasov.tm.api.model.ICommand;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.exception.AbstractException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    @Override
    public abstract void execute() throws AbstractException;

    @Override
    public abstract String getName();

    @Override
    public abstract String getArgument();

    @Override
    public abstract String getDescription();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null &&!argument.isEmpty()) result += ": " + description;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
